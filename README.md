# stargazer

stores stargazer resources for clinical bioinformatics pipeline

## Notes

May 5, 2022: Stargazer Updates

1) SLCO1B1 *17 will not be called anymore.
2) The phenotype table had a couple of typos that have been corrected.

updated pipeline files recevied May 3, 2022 from Aparna Radhakrishnan at UW, contact: aparnark@uw.edu

July 27, 2022: PGx QC Updates

Steps Added:
1) Convert the 38 gvcf to vcf for a bed file using bcftools
2) Check the depth and genotype quality for all variants in a gene.

- The method by which stargazer makes calls is not changed with this update. 
- Site quality thresholds: DP>=20 and GQ>=20 (DP>=10 and GQ>=20 for G6PD in XY-ploidy samples).
- If any allele-defining core variants on a gene do not pass these QC metrics, the gene will be labeled "indeterminate". 
